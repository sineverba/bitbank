import * as React from "react";
import Avatar from "@mui/material/Avatar";
import CssBaseline from "@mui/material/CssBaseline";
import Link from "@mui/material/Link";
import Box from "@mui/material/Box";
import AccountBalanceIcon from "@mui/icons-material/AccountBalance";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Feedback } from "@sineverba/feedback";
import { Loading } from "@sineverba/loading";
import { usePostLoginMutation } from "./store/apiSlice";
import LoginForm from "./views/LoginPage/LoginForm";

export const App = () => {
  const [postLogin, { data, isLoading }] = usePostLoginMutation();

  const handleClick = (event) => {
    event.preventDefault();
    console.log(event.currentTarget);
    const formData = new FormData(event.currentTarget);
    postLogin({
      username: formData.get("username"),
      password: formData.get("password")
    });
  };

  return (
    <ThemeProvider theme={createTheme()}>
      <Feedback />
      <Feedback message="OK!" />
      <Feedback isError />
      {isLoading && <Loading />}
      {!isLoading && (
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Box
            sx={{
              marginTop: 8,
              display: "flex",
              flexDirection: "column",
              alignItems: "center"
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
              <AccountBalanceIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign in
            </Typography>
            {(!data ||
              !Object.prototype.hasOwnProperty.call(data, "castle_id")) && (
              <LoginForm handleClick={handleClick} />
            )}
          </Box>
          <Typography
            variant="body2"
            color="text.secondary"
            align="center"
            sx={{ mt: 8, mb: 4 }}
          >
            {"Copyright © "}
            <Link color="inherit" href="https://mui.com/">
              Your Website
            </Link>{" "}
            {new Date().getFullYear()}
          </Typography>
        </Container>
      )}
    </ThemeProvider>
  );
};

export default App;
