import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { prepareHeaders } from "../utils/storeMethods";

const baseQuery = fetchBaseQuery({
  baseUrl: process.env.REACT_APP_BACKEND_URL,
  prepareHeaders: (headers) => {
    prepareHeaders(headers);
  }
});

const baseQueryHandling401 = async (args, api, extraOptions) => {
  const result = await baseQuery(args, api, extraOptions);
  if (result.error && result.error.status === 401) {
    return null;
  }
  return result;
};

export const apiSlice = createApi({
  // Reducer Path it's name shown on Redux Tab
  reducerPath: "apiSlice",
  baseQuery: baseQueryHandling401,
  // With tag type we can invalidate cache
  tagTypes: ["LOGIN"],
  endpoints: (builder) => ({
    /**
     * Login section
     */
    postLogin: builder.mutation({
      query: (body) => ({
        url: "/v2/auth/login",
        method: "POST",
        body
      }),
      providesTags: ["LOGIN"]
    })
  })
});
/**
 * Names export are endpoints: use{endpoint}Query
 */
export const { usePostLoginMutation } = apiSlice;
