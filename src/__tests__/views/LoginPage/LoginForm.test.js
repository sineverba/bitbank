import { act, fireEvent, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import LoginForm from "../../../views/LoginPage/LoginForm";
import { renderWithProviders } from "../../__test-utils__/test-utils";

describe("Test App", () => {
  let usernameInputText;
  let passwordInputText;
  let loginButton;


  const setup = () => {
    renderWithProviders(<LoginForm handleSubmit={() => {}}/>);
    // Form Section
    usernameInputText = screen.getByLabelText(/username/i);
    passwordInputText = screen.getByLabelText(/password/i);
    loginButton = screen.getByRole("button", { name: /login/i });
  };

  it("Can handle login form", async () => {
    setup();
    const user = userEvent.setup();
    expect(usernameInputText).toBeInTheDocument();
    expect(passwordInputText).toBeInTheDocument();
    expect(loginButton).toBeInTheDocument();

    await act(async () => {
      fireEvent.change(usernameInputText, {
        target: { value: "foo@example.com" }
      });
    });

    expect(usernameInputText).toHaveValue("foo@example.com");

    await act(async () => {
      fireEvent.change(passwordInputText, {
        target: { value: "randomPassword" }
      });
    });

    expect(passwordInputText).toHaveValue("randomPassword");

    await act(async () => {
      loginButton.click();
    });

    expect(usernameInputText).not.toBeInTheDocument();
  });
});
