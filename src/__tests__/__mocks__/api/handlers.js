import { rest } from "msw";
import { item as auth } from "../responses/auth";
export const handlers = [
  // Login
  rest.post(
    `${process.env.REACT_APP_BACKEND_URL}/v2/auth/login`,
    (req, res, ctx) => {
      return res(ctx.delay(), ctx.status(200), ctx.json(auth));
    }
  )
];
