import { act, fireEvent, screen } from "@testing-library/react";
import { renderWithProviders } from "../__tests__/__test-utils__/test-utils";
import userEvent from "@testing-library/user-event";
import App from "../App";

describe("Test App", () => {
  let copyright;
  let usernameInputText;
  let passwordInputText;
  let loginButton;

  const setup = () => {
    renderWithProviders(<App />);
    copyright = screen.getByText(/copyright/i);
    // Form Section
    usernameInputText = screen.getByLabelText(/username/i);
    passwordInputText = screen.getByLabelText(/password/i);
    loginButton = screen.getByRole("button", { name: /login/i });
  };

  it("Can render copyright", async () => {
    setup();
    expect(copyright).toBeInTheDocument();
  });

  it("Can handle login form", async () => {
    setup();
    const user = userEvent.setup();
    expect(usernameInputText).toBeInTheDocument();
    expect(passwordInputText).toBeInTheDocument();
    expect(loginButton).toBeInTheDocument();

    await act(async () => {
      fireEvent.change(usernameInputText, {
        target: { value: "foo@example.com" }
      });
    });

    expect(usernameInputText).toHaveValue("foo@example.com");

    await act(async () => {
      fireEvent.change(passwordInputText, {
        target: { value: "randomPassword" }
      });
    });

    expect(passwordInputText).toHaveValue("randomPassword");

    await act(async () => {
      loginButton.click();
    })
  });
});
