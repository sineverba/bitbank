import index from "../index";

describe("Test index.jsx", () => {
  it("Should render app without crashing", () => {
    expect(
      JSON.stringify(
        Object.assign({}, index, { _reactInternalInstance: "censored" })
      )
    ).toMatchSnapshot();
  });
});
