import { Box, Button, TextField } from "@mui/material";
import React from "react";

const LoginForm = ({handleClick}) => (
  <Box component="form" noValidate sx={{ mt: 1 }}>
    <TextField
      margin="normal"
      required
      fullWidth
      id="username"
      label="Username"
      name="username"
      autoComplete="username"
      autoFocus
    />
    <TextField
      margin="normal"
      required
      fullWidth
      name="password"
      label="Password"
      type="password"
      id="password"
      autoComplete="current-password"
    />
    <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }} onClick={handleClick}>
      Login
    </Button>
  </Box>
);

export default LoginForm;
