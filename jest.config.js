const config = {
  testMatch: [
    "**/__tests__/**/*.js",
    "!**/__mocks__/**",
    "!**/__test-utils__/**"
  ],
  resetMocks: true,
  clearMocks: true,
  setupFilesAfterEnv: ["<rootDir>/src/setupTests.js"],
  testEnvironment: "jsdom",
  collectCoverageFrom: ["<rootDir>/src/**/*.jsx"],
  coverageDirectory: "<rootDir>/report",
  moduleNameMapper: {
    "\\.(css|less|scss)$": "<rootDir>/src/__tests__/__mocks__/jest/styleMock.js"
  }
};

module.exports = config;
