TODO
====
+ Rimuovere da package setup dotenv e portarlo in webpack o in jest  https://stackoverflow.com/questions/50259025/using-env-files-for-unit-testing-with-jest
+ Aggiungere Log
+ Sistemare webpack con separazione mode (https://webpack.js.org/guides/production/)
+ Portare in dist anche favicon, robots etc
+ Accettare come env solo con prefisso REACT_APP
+ Aggiungere Sonar
+ Aggiungere Semaphore
+ Aggiungere CircleCI
+ Aggiungere Makefile
+ Aggiungere docker