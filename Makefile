include .env

IMAGE_NAME=react-wo-cra
CONTAINER_NAME=react-wo-cra
VERSION=0.3.0-dev
NODE_VERSION=18.16.1
NPM_VERSION=9.8.0
NGINX_VERSION=1.25.1
SONARSCANNER_VERSION=4.8.0
SONARSCANNER_NETWORK=sonarqube

sonar:
	docker network create --driver bridge $(SONARSCANNER_NETWORK) || true
	docker run --rm -it \
		--name sonarscanner \
		-v $(PWD):/usr/src \
		sonarsource/sonar-scanner-cli:$(SONARSCANNER_VERSION)

upgrade:
	npx ncu -u
	npx browserslist@latest --update-db
	npm install
	npm audit fix

fixnodesass:
	npm rebuild node-sass

build:
	docker build \
		--build-arg NODE_VERSION=$(NODE_VERSION) \
		--build-arg NPM_VERSION=$(NPM_VERSION) \
		--tag $(IMAGE_NAME):$(VERSION) \
		--file dockerfiles/production/build/docker/Dockerfile \
		"."

test:
	docker run --rm -it $(IMAGE_NAME):$(VERSION) cat /etc/os-release | grep "Alpine Linux v3.17"
	docker run --rm -it $(IMAGE_NAME):$(VERSION) cat /etc/os-release | grep "VERSION_ID=3.17.4"
	docker run --rm -it $(IMAGE_NAME):$(VERSION) nginx -v | grep $(NGINX_VERSION)
	
spin:
	docker container run -it --rm --publish 8080:80 --name $(CONTAINER_NAME) $(IMAGE_NAME):$(VERSION)

destroy:
	docker image rm node:$(NODE_VERSION)-alpine3.18
	docker image rm nginx:$(NGINX_VERSION)-alpine3.17-slim
	docker image rm $(IMAGE_NAME):$(VERSION)